namespace buh
{
    partial class RaspredTIKreport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RaspredTIKreport));
            this.kadryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buhDataSet = new buh.buhDataSet();
            this.tixBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RaspredTIKBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tixTableAdapter = new buh.buhDataSetTableAdapters.tixTableAdapter();
            this.kadryTableAdapter = new buh.buhDataSetTableAdapters.kadryTableAdapter();
            this.grafikBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grafikTableAdapter = new buh.buhDataSetTableAdapters.grafikTableAdapter();
            this.datesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.datesTableAdapter = new buh.buhDataSetTableAdapters.datesTableAdapter();
            this.GrafikUIKBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.kadryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buhDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tixBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RaspredTIKBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafikBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrafikUIKBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // kadryBindingSource
            // 
            this.kadryBindingSource.DataMember = "kadry";
            this.kadryBindingSource.DataSource = this.buhDataSet;
            // 
            // buhDataSet
            // 
            this.buhDataSet.DataSetName = "buhDataSet";
            this.buhDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tixBindingSource
            // 
            this.tixBindingSource.DataMember = "tix";
            this.tixBindingSource.DataSource = this.buhDataSet;
            // 
            // RaspredTIKBindingSource
            // 
            this.RaspredTIKBindingSource.DataMember = "RaspredTIK";
            this.RaspredTIKBindingSource.DataSource = this.buhDataSet;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "buhDataSet_kadry";
            reportDataSource1.Value = this.kadryBindingSource;
            reportDataSource2.Name = "buhDataSet_tix";
            reportDataSource2.Value = this.tixBindingSource;
            reportDataSource3.Name = "buhDataSet_RaspredTIK";
            reportDataSource3.Value = this.RaspredTIKBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource3);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "buh.reports.������������� ���.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ShowBackButton = false;
            this.reportViewer1.ShowContextMenu = false;
            this.reportViewer1.ShowCredentialPrompts = false;
            this.reportViewer1.ShowDocumentMapButton = false;
            this.reportViewer1.ShowFindControls = false;
            this.reportViewer1.ShowRefreshButton = false;
            this.reportViewer1.ShowStopButton = false;
            this.reportViewer1.ShowZoomControl = false;
            this.reportViewer1.Size = new System.Drawing.Size(784, 602);
            this.reportViewer1.TabIndex = 0;
            // 
            // tixTableAdapter
            // 
            this.tixTableAdapter.ClearBeforeFill = true;
            // 
            // kadryTableAdapter
            // 
            this.kadryTableAdapter.ClearBeforeFill = true;
            // 
            // grafikBindingSource
            // 
            this.grafikBindingSource.DataMember = "grafik";
            this.grafikBindingSource.DataSource = this.buhDataSet;
            // 
            // grafikTableAdapter
            // 
            this.grafikTableAdapter.ClearBeforeFill = true;
            // 
            // datesBindingSource
            // 
            this.datesBindingSource.DataMember = "dates";
            this.datesBindingSource.DataSource = this.buhDataSet;
            // 
            // datesTableAdapter
            // 
            this.datesTableAdapter.ClearBeforeFill = true;
            // 
            // GrafikUIKBindingSource
            // 
            this.GrafikUIKBindingSource.DataMember = "GrafikUIK";
            this.GrafikUIKBindingSource.DataSource = this.buhDataSet;
            // 
            // RaspredTIKreport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 602);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RaspredTIKreport";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "������������� ���";
            this.Load += new System.EventHandler(this.viewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.kadryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buhDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tixBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RaspredTIKBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafikBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrafikUIKBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource tixBindingSource;
        private buhDataSet buhDataSet;
        private buh.buhDataSetTableAdapters.tixTableAdapter tixTableAdapter;
        private System.Windows.Forms.BindingSource kadryBindingSource;
        private buh.buhDataSetTableAdapters.kadryTableAdapter kadryTableAdapter;
        private System.Windows.Forms.BindingSource RaspredTIKBindingSource;
        private System.Windows.Forms.BindingSource grafikBindingSource;
        private System.Windows.Forms.BindingSource datesBindingSource;
        private System.Windows.Forms.BindingSource GrafikUIKBindingSource;
        private buh.buhDataSetTableAdapters.grafikTableAdapter grafikTableAdapter;
        private buh.buhDataSetTableAdapters.datesTableAdapter datesTableAdapter;

    }
}