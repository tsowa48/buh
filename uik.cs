using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace buh
{
    public partial class uik : Form
    {
        const String TITLE = "��������� ���";
        private int uikNum = -1;
        private int minMonth = 0;
        private int maxMonth = 0;
        private int year = 0;

        public uik()
        {
            InitializeComponent();
        }

        private void uik_Load(object sender, EventArgs e)
        {
            mdb.open();
            util.InputComboBox icb = new util.InputComboBox();
            icb.Text = TITLE;
            //��������� ������ ���
            icb.label1.Text = "�������� ��������������� ��������:";
            DataTable dt = mdb.exec("select name,iknum from tix order by iknum;").Tables[0];
            for (int i = 0; i < dt.Rows.Count; ++i)
                icb.comboBox1.Items.Add("��� " + dt.Rows[i][0].ToString());
            icb.comboBox1.SelectedIndex = 0;
            if (DialogResult.OK == icb.ShowDialog() && icb.comboBox1.SelectedIndex > -1)
            {
                int iknum = Convert.ToInt32(dt.Rows[icb.comboBox1.SelectedIndex][1]);
                //��������� ������ ��� ��������� ���
                icb.label1.Text = "�������� ���������� ��������:";
                dt = mdb.exec("select iknum from ik where iknum>=" + Convert.ToString(iknum * 100) + " and iknum<" + Convert.ToString((iknum + 1) * 100) + " order by iknum;").Tables[0];
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("��� ������ ��������������� �������� ��� ��������.");
                    mdb.close();
                    Application.Exit();
                }
                icb.comboBox1.Items.Clear();
                for (int i = 0; i < dt.Rows.Count; ++i)
                    icb.comboBox1.Items.Add("���������� ������������� �������� �" + String.Format("{0:00-00}",dt.Rows[i][0]));
                icb.comboBox1.SelectedIndex = 0;
                if (DialogResult.OK == icb.ShowDialog() && icb.comboBox1.SelectedIndex > -1)
                {
                    iknum = Convert.ToInt32(dt.Rows[icb.comboBox1.SelectedIndex][0]);
                    uikNum = iknum;
                    //���������� ������ � �������� ���
                    dt = mdb.exec("select fio,post,koef from kadry where iknum=" + iknum.ToString() + " order by post asc;").Tables[0];
                    int workers = dt.Rows.Count;//���������� ������ ���

                    for (int i = 0; i < workers; ++i)
                    {
                        String[] fio = dt.Rows[i][0].ToString().Split(' ');
                        int post = Convert.ToInt32(dt.Rows[i][1]);
                        String koef = dt.Rows[i][2] is DBNull ? "1.5" : Convert.ToDouble(dt.Rows[i][2]).ToString("0.0").Replace(",", ".");
                        DataGridViewRow row = new DataGridViewRow();
                        DataGridViewTextBoxCell cell1 = new DataGridViewTextBoxCell();
                        DataGridViewTextBoxCell cell2 = new DataGridViewTextBoxCell();
                        DataGridViewTextBoxCell cell3 = new DataGridViewTextBoxCell();
                        DataGridViewTextBoxCell cell4 = new DataGridViewTextBoxCell();
                        DataGridViewComboBoxCell cell5 = new DataGridViewComboBoxCell();
                        cell1.Value = i + 1;
                        cell2.Value = dt.Rows[i][0].ToString();
                        cell3.Value = fio[0] + " " + fio[1].Substring(0, 1) + ". " + fio[2].Substring(0, 1) + ".";
                        cell4.Value = post == 0 ? "������������" : (post == 1 ? "�����������" : (post == 2 ? "���������" : "���� ��������"));
                        cell5.FlatStyle = FlatStyle.Flat;
                        cell5.Items.AddRange("0.0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "2.0");
                        row.Cells.AddRange(cell1, cell2, cell3, cell4, cell5);
                        if (post == 0)
                        {
                            cell5.ReadOnly = true;
                            cell5.Value = "0.0";
                        }
                        else
                            cell5.Value = koef;
                        
                        dataGridView1.Rows.Add(row);
                        dataGridView2.Columns.Add("uik" + i.ToString(), dt.Rows[i][0].ToString());//���������� ��� � ������� �������
                        dataGridView3.Columns.Add("uik" + i.ToString(), dt.Rows[i][0].ToString());//���������� ��� � ������� ��������
                    }

                    //���������� ������� ������
                    DataTable dt1 = mdb.exec("select R.[day], R.holiday from rezhim R order by R.[day] asc;").Tables[0];
                    String vote = Convert.ToDateTime(mdb.exec("select vote from dates;").Tables[0].Rows[0][0]).ToShortDateString();
                    bool isGraficate = false;//����, ���� � �� ��� ���� ������
                    for (int i = 0; i < dt1.Rows.Count; ++i)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.HeaderCell.Value = Convert.ToDateTime(dt1.Rows[i][0]).ToLongDateString();
                        if (Convert.ToBoolean(dt1.Rows[i][1]))//���� �������� ��� ��������
                        {
                            DataGridViewCellStyle style = row.HeaderCell.Style;
                            style.BackColor = Color.Pink;
                            row.HeaderCell.Style.ApplyStyle(style);
                            row.DefaultCellStyle.BackColor = Color.Pink;
                        }
                        for (int j = 0; j < workers; ++j)
                        {
                            DataGridViewComboBoxCell cell = new DataGridViewComboBoxCell();
                            cell.FlatStyle = FlatStyle.Flat;
                            if (Convert.ToDateTime(dt1.Rows[i][0]).ToShortDateString() == vote)//���������� ����� ��� �����������
                                cell.Items.AddRange("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18");//������������ ����
                            else if (Convert.ToDateTime(dt1.Rows[i][0]) == Convert.ToDateTime(vote).AddDays(1))
                                cell.Items.AddRange("0", "1", "2", "3", "4", "5", "6");//������������ ���� �� ���� ����� �����������
                            else
                                cell.Items.AddRange("0", "1", "2", "3", "4", "5", "6", "7", "8");//������������ ����

                            //�������� �������� ����� �� ��
                            DataTable dt2 = mdb.exec("select G.[hour] from grafik G where G.iknum=" + uikNum.ToString() + " and G.[day]=CDate('" + Convert.ToDateTime(dt1.Rows[i][0]).ToShortDateString() + "') and G.fio='" + dt.Rows[j][0].ToString() + "';").Tables[0];
                            if (dt2.Rows.Count > 0)
                            {
                                cell.Value = dt2.Rows[0][0].ToString();
                                isGraficate = true;
                            }
                            else
                                cell.Value = cell.Items[0];
                            row.Cells.Add(cell);
                        }
                        dataGridView2.Rows.Add(row);
                    }
                    
                    //���������� ���� ��� ������� �� ������� (������ ������)
                    List<ToolStripMenuItem> monthsG = new List<ToolStripMenuItem>();//������ ��� ���� "������"
                    List<ToolStripMenuItem> monthsSved = new List<ToolStripMenuItem>();//������ ��� ���� "��������"
                    //List<ToolStripMenuItem> monthsSvod = new List<ToolStripMenuItem>();//������ ��� ���� "�������"
                    int curMonth = Convert.ToDateTime(dt1.Rows[0][0]).Month;
                    minMonth = curMonth;
                    year = Convert.ToDateTime(dt1.Rows[0][0]).Year;
                    
                    monthsG.Add(new ToolStripMenuItem("�� " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(curMonth).ToLower(), null, �����ToolStripMenuItem_Click));
                    monthsSved.Add(new ToolStripMenuItem("�� " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(curMonth).ToLower(), null, ������������ToolStripMenuItem_Click));
                    //monthsSvod.Add(new ToolStripMenuItem("�� " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(curMonth).ToLower(), null, ������������ToolStripMenuItem1_Click));

                    for (int i = 0; i < dt1.Rows.Count; ++i)
                    {
                        int newMonth = Convert.ToDateTime(dt1.Rows[i][0]).Month;
                        if (curMonth != newMonth)
                        {
                            curMonth = newMonth;
                            monthsG.Add(new ToolStripMenuItem("�� " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(curMonth).ToLower(), null, �����ToolStripMenuItem_Click));
                            monthsSved.Add(new ToolStripMenuItem("�� " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(curMonth).ToLower(), null, ������������ToolStripMenuItem_Click));
                            //monthsSvod.Add(new ToolStripMenuItem("�� " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(curMonth).ToLower(), null, ������������ToolStripMenuItem1_Click));
                        }
                    }
                    maxMonth = curMonth;
                    ������������ToolStripMenuItem.DropDownItems.AddRange(monthsG.ToArray());
                    ��������ToolStripMenuItem.DropDownItems.AddRange(monthsSved.ToArray());
                    //�������ToolStripMenuItem.DropDownItems.AddRange(monthsSvod.ToArray());

                    //���������� �������� �� ������������ ����� ("��������")
                    bool isSved = false;//����, ���� � �� ��� ���� ��������
                    for (int i = 0; i < dt1.Rows.Count; ++i)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.HeaderCell.Value = Convert.ToDateTime(dt1.Rows[i][0]).ToLongDateString();
                        if (Convert.ToBoolean(dt1.Rows[i][1]))//���� �������� ��� ��������
                        {
                            DataGridViewCellStyle style = row.HeaderCell.Style;
                            style.BackColor = Color.Pink;
                            row.HeaderCell.Style.ApplyStyle(style);
                            row.DefaultCellStyle.BackColor = Color.Pink;
                        }
                        for (int j = 0; j < workers; ++j)
                        {
                            DataGridViewComboBoxCell cell = new DataGridViewComboBoxCell();
                            cell.FlatStyle = FlatStyle.Flat;
                            if (Convert.ToDateTime(dt1.Rows[i][0]).ToShortDateString() == vote)//������ ������ � ���� �����������
                                cell.Items.AddRange("� 6:00", "� 7:00", "� 8:00", "� 9:00", "� 10:00", "� 11:00", "� 12:00", "� 13:00", "� 14:00", "� 15:00", "� 16:00", "� 17:00", "� 18:00", "� 19:00", "� 20:00", "� 21:00", "� 22:00", "� 23:00");//���� ������ ������
                            else if (Convert.ToDateTime(dt1.Rows[i][0]) == Convert.ToDateTime(vote).AddDays(1))
                                cell.Items.AddRange("� 0:00", "� 1:00", "� 2:00", "� 3:00", "� 4:00", "� 5:00");//������ ������ � ���� ����� �����������
                            else
                                cell.Items.AddRange("� 10:00", "� 11:00", "� 12:00", "� 13:00", "� 14:00", "� 15:00", "� 16:00", "� 17:00");//������ ������

                            //�������� �������� ����� �� ��
                            DataTable dt2 = mdb.exec("select S.sHour from sved S where S.iknum=" + uikNum.ToString() + " and S.[day]=CDate('" + Convert.ToDateTime(dt1.Rows[i][0]).ToShortDateString() + "') and S.fio='" + dt.Rows[j][0].ToString() + "';").Tables[0];
                            if (dt2.Rows.Count > 0)
                            {
                                cell.Value = "� " + dt2.Rows[0][0].ToString() + ":00";
                                isSved = true;
                            }
                            else
                                cell.Value = cell.Items[0];
                            row.Cells.Add(cell);
                        }
                        dataGridView3.Rows.Add(row);
                    }


                    tabControl1.TabPages.Remove(tabPage3);
                    if (isGraficate)//���� � �� ���� "������"
                    {
                        ��������ToolStripMenuItem.Enabled = true;//��������� ������������ � ������ ������� ������
                        tabControl1.TabPages.Add(tabPage3);
                        tabPage3.Show();//�������� ������� "��������"
                    }
                    if (isSved)//���� � �� ���� "��������"
                    {
                        �������ToolStripMenuItem.Enabled = true;//��������� ������������ � ������ ������� ������
                        ���������ToolStripMenuItem.Enabled = true;
                        ��������������ToolStripMenuItem.Enabled = true;
                    }
                }
                else
                {
                    mdb.close();
                    Application.Exit();
                }
            }
            else
            {
                mdb.close();
                Application.Exit();
            }
            mdb.close();
        }

        private void uik_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        private void �����ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mdb.open();

            //�������� ������ ���������� ����� �� �������� � ������ ��������� � ������ �����������/����������� �����")
            int voters = Convert.ToInt32(mdb.exec("select I.voters from ik I where I.iknum=" + uikNum.ToString() + ";").Tables[0].Rows[0][0]);
            int group = voters < 1001 ? 1 : (voters > 2000 ? 3 : 2);
            for (int C = 0; C < dataGridView2.ColumnCount; ++C)
            {
                String fio = dataGridView2.Columns[C].HeaderText;//��������� �������� ������� (���)
                int post = C > 3 ? 3 : C;
                int SumHours = 0;
                int HoSumHours = 0;
                for (int R = 0; R < dataGridView2.RowCount; ++R)//������ �� �������
                {
                    if (dataGridView2[C, R].InheritedStyle.BackColor == Color.Pink)
                        HoSumHours += Convert.ToInt32(dataGridView2[C, R].Value);
                    else
                        SumHours += Convert.ToInt32(dataGridView2[C, R].Value);
                }
                if (SumHours > 0 || HoSumHours > 0)
                {
                    DataTable dtb = mdb.exec("select R.hour from rabchas R where R.grp=" + group.ToString() + " and post=" + post.ToString() + " order by R.day asc;").Tables[0];
                    int dHours = Convert.ToInt32(dtb.Rows[0][0]);
                    int dhHours = Convert.ToInt32(dtb.Rows[1][0]);
                    if (SumHours > dHours)
                    {
                        MessageBox.Show("��������� ���������� ����� � �����!\n\n���������: " + fio + "\n�������: " + SumHours.ToString() + "\n��������: " + dHours.ToString());
                        mdb.close();
                        return;
                    }
                    if (HoSumHours > dhHours)
                    {
                        MessageBox.Show("��������� ���������� ����� � ��������!\n\n���������: " + fio + "\n�������: " + HoSumHours.ToString() + "\n��������: " + dhHours.ToString());
                        mdb.close();
                        return;
                    }
                }
            }
            //���������� � ������� grafik
            mdb.exec("delete from grafik where iknum=" + uikNum.ToString() + ";");
            for (int cols = 0; cols < dataGridView2.ColumnCount; ++cols)//������ �� ��������
            {
                String fio = dataGridView2.Columns[cols].HeaderText;//��������� �������� ������� (���)
                for (int rows = 0; rows < dataGridView2.RowCount; ++rows)//������ �� �������
                {
                    DateTime day = Convert.ToDateTime(dataGridView2.Rows[rows].HeaderCell.Value);//��������� �������� ������ (����)
                    int hour = Convert.ToInt32(dataGridView2[cols, rows].Value);
                    mdb.exec("insert into grafik(iknum,fio,[day],[hour]) values(" + uikNum.ToString() + ",'" + fio + "','" + day + "'," + hour.ToString() + ");");
                }
            }
            mdb.close();
            String period = sender.ToString().Replace("�� ", "");//������ ������

            ��������ToolStripMenuItem.Enabled = true;//��������� ������������ � ������ ������� ������
            tabControl1.TabPages.Remove(tabPage3);
            //��������� ������� "��������"
            tabControl1.TabPages.Add(tabPage3);
            tabPage3.Show();

            //����� ����� ������
            (new GrafikUIKreport(new object[] { uikNum, minMonth, maxMonth, year, period })).ShowDialog();
        }
        private void ������������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //���������� � ������� sved
            mdb.open();
            mdb.exec("delete from sved where iknum=" + uikNum.ToString() + ";");
            for (int cols = 0; cols < dataGridView3.ColumnCount; ++cols)//������ �� ��������
            {
                String fio = dataGridView3.Columns[cols].HeaderText;//��������� �������� ������� (���)
                for (int rows = 0; rows < dataGridView3.RowCount; ++rows)//������ �� �������
                {
                    DateTime day = Convert.ToDateTime(dataGridView3.Rows[rows].HeaderCell.Value);//��������� �������� ������ (����)
                    int sHour = Convert.ToInt32(dataGridView3[cols, rows].Value.ToString().Replace("� ", "").Replace(":00", ""));//����� ������ ������
                    mdb.exec("insert into sved(iknum,fio,[day],sHour) values(" + uikNum.ToString() + ",'" + fio + "','" + day + "'," + sHour.ToString() + ");");
                }
            }
            mdb.close();
            String period = sender.ToString().Replace("�� ", "");//������ ������

            �������ToolStripMenuItem.Enabled = true;//��������� ������������ � ������ ������� ������
            ���������ToolStripMenuItem.Enabled = true;
            ��������������ToolStripMenuItem.Enabled = true;

            //����� ����� ������
            (new SvedUIKreport(new object[] { uikNum, minMonth, maxMonth, year, period })).ShowDialog();
        }
        private void ���������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //����� ����� ������
            (new DopOplataUIKreport(new object[] { uikNum, 1 })).ShowDialog();
        }
        private void ��������������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //����� ����� ������
            (new DopOplataUIKreport(new object[] { uikNum, 2 })).ShowDialog();
        }
        private void ����������������������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // TODO: �������� ��� ���
        }
        private void ������������ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //����� ����� ������
            new RaschetUIKreport(new object[] { uikNum }).ShowDialog();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex > 0)
            {
                //��������� �������� Koef � ������� Kadry
                mdb.open();
                int cnt = dataGridView1.Rows.Count;
                for (int r = 0; r < cnt; ++r)
                {
                    String koef = dataGridView1.Rows[r].Cells[4].Value == null ? "1.5" : dataGridView1.Rows[r].Cells[4].Value.ToString();
                    mdb.exec("update kadry K set K.koef=" + koef + " where K.iknum=" + uikNum.ToString() + " and K.fio='" + dataGridView1.Rows[r].Cells[1].Value.ToString() + "';");
                }
                mdb.close();
            }
        }
    }
}