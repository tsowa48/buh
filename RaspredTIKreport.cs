using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace buh
{
    public partial class RaspredTIKreport : Form
    {
        //private object[] param = null;

        public RaspredTIKreport()
        {
            InitializeComponent();
        }
        private void viewer_Load(object sender, EventArgs e)
        {
            mdb.open();
            DataTable ds0 = mdb.exec("select T.iknum, T.name from tix T order by T.iknum;").Tables[0];
            for (int t = 0; t < ds0.Rows.Count; ++t)
            {
                DataTable ds = mdb.exec("select T.name, S.grp, S.price, count(K.iknum), I.iknum from ik I, tix T, stavka S, kadry K where K.iknum=I.iknum and I.iknum>=T.iknum*100 and I.iknum<(T.iknum+1)*100 and T.iknum=" + ds0.Rows[t][0].ToString() + " and iif(I.voters<1001, 1, iif(I.voters>2000, 3, 2))=S.grp group by T.name, S.grp, S.price, I.iknum order by I.iknum asc;").Tables[0];
                Double AllVoznagrazhdeniePredsedateley = 0.0;
                Double DopOplataTruda = 0.0;
                for (int i = 0; i < ds.Rows.Count; ++i)//������ �� ������ ���
                {
                    DataTable ds1 = mdb.exec("select sum((R.[day]+1)*R.hour),R.post from rabchas R where R.grp=" + ds.Rows[i][1].ToString() + " group by R.post order by R.post asc;").Tables[0];
                    Double DopOplataPredsedateley = Convert.ToDouble(ds.Rows[i][2]) * Convert.ToDouble(ds1.Rows[0][0]);
                    Double DopOplataChlenov = Convert.ToDouble(ds.Rows[i][2]) * ((Convert.ToDouble(ds1.Rows[1][0]) + Convert.ToDouble(ds1.Rows[2][0])) * 0.9 +
                        Convert.ToDouble(ds1.Rows[3][0]) * 0.7 * Convert.ToDouble(Convert.ToInt32(ds.Rows[i][3]) - 3));
                    Double VoznagrazhdenieChlenov = DopOplataChlenov * 1.5;//�������������� ������ �������� (150%)
                    AllVoznagrazhdeniePredsedateley += DopOplataPredsedateley * 1.5;//�������������� ������������� �������� (150%)
                    Double dopOpl = DopOplataPredsedateley + DopOplataChlenov + VoznagrazhdenieChlenov;
                    DopOplataTruda += dopOpl;
                    mdb.exec("delete from raspred where iknum=" + ds.Rows[i][4].ToString() + ";");
                    mdb.exec("insert into raspred(iknum,dopopl,vozn) values(" + ds.Rows[i][4].ToString() + "," + Convert.ToString(dopOpl).Replace(",", ".") + "," + Convert.ToString(DopOplataPredsedateley * 1.5).Replace(",", ".") + ");");
                }
                mdb.exec("delete from raspred where iknum=" + Convert.ToString(Convert.ToInt32(ds0.Rows[t][0]) * 100) + ";");
                mdb.exec("insert into raspred(iknum,dopopl,vozn) values(" + Convert.ToString(Convert.ToInt32(ds0.Rows[t][0]) * 100) + "," + Convert.ToString(DopOplataTruda).Replace(",", ".") + "," + Convert.ToString(AllVoznagrazhdeniePredsedateley).Replace(",", ".") + ");");
                buhDataSet.RaspredTIK.Rows.Add(ds0.Rows[t][1].ToString(), DopOplataTruda, AllVoznagrazhdeniePredsedateley);
            }
            mdb.close();
            
            //System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            //pg.Margins.Top = 0;
            //pg.Margins.Bottom = 0;
            //pg.Margins.Left = 0;
            //pg.Margins.Right = 0;
            //System.Drawing.Printing.PaperSize size = new System.Drawing.Printing.PaperSize();
            //size.RawKind = (int)System.Drawing.Printing.PaperKind.A4;
            //pg.PaperSize = size;
            //reportViewer1.SetPageSettings(pg);

            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);//Show as PrintPage
            this.reportViewer1.RefreshReport();
        }
    }
}