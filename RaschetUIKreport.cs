using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace buh
{
    public partial class RaschetUIKreport : Form
    {
        private object[] param;

        public RaschetUIKreport()
        {
            InitializeComponent();
        }
        public RaschetUIKreport(object[] param)
        {
            InitializeComponent();
            this.param = param;
        }

        private void RaschetUIKreport_Load(object sender, EventArgs e)
        {
            int iknum = Convert.ToInt32(this.param[0]);
            String[] month = new String[] { "-", "������", "�������", "����", "������", "���", "����", "����", "������", "��������", "�������", "������", "�������" };
            mdb.open();
            String tikName = mdb.exec("select T.name from tix T where T.iknum=" + Math.Floor(iknum/100.0) + ";").Tables[0].Rows[0][0].ToString();
            DataRow period_ = mdb.exec("select month(D.start), month(D.stop) from dates D;").Tables[0].Rows[0];
            String period = month[Convert.ToInt32(period_[0])] + " - " + month[Convert.ToInt32(period_[1])];
            Double stavka = Convert.ToDouble(mdb.exec("select S.price from stavka S, ik I where I.iknum=" + iknum.ToString() + " and iif(I.voters<1001, 1, iif(I.voters>2000, 3, 2))=S.grp;").Tables[0].Rows[0][0]);
            DataTable dt = mdb.exec("select K.fio, K.post, K.koef from kadry K where iknum=" + iknum.ToString() + " order by K.post asc, K.fio asc;").Tables[0];
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                int nPost = Convert.ToInt32(dt.Rows[r][1]);
                String post = (nPost == 0 ? "������������" : (nPost == 1 ? "���. ������������" : (nPost == 2 ? "���������" : "���� ��������")));
                String fio = dt.Rows[r][0].ToString();
                Double coef = (nPost == 1 || nPost == 2) ? 0.9 : (nPost == 3 ? 0.7 : 1.0);

                //��� ����
                int allHours = 0;
                allHours = Convert.ToInt32(mdb.exec("select Sum(G.[hour]) from rezhim R, sved S, grafik G, kadry K where S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and S.fio=K.fio and S.fio='" + fio + "' and K.iknum=" + iknum.ToString() + " group by S.fio;").Tables[0].Rows[0][0]);
                //����� �� ������ �����
                int nightHours = 0;
                DataTable dt1 = mdb.exec("select G.[hour], S.sHour from rezhim R, sved S, grafik G where S.iknum=" + iknum.ToString() + " and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and ((S.sHour<6 and G.[hour]>0) or (S.sHour+G.[hour])>21) and S.fio='" + fio + "';").Tables[0];
                for (int r1 = 0; r1 < dt1.Rows.Count; ++r1)
                {
                    int hour = Convert.ToInt32(dt1.Rows[r1][0]);
                    int sHour = Convert.ToInt32(dt1.Rows[r1][1]);
                    if ((sHour + hour) > 21)
                        nightHours += (sHour + hour) - 22;
                    else if (sHour < 6 && hour > 0 && (sHour + hour < 7))
                        nightHours += hour;
                    else if (sHour < 6 && hour > 0 && (sHour + hour > 7))
                        nightHours += hour - (sHour + hour - 6);
                }
                //����� �� ��������
                int holyHours = 0;
                holyHours = Convert.ToInt32(mdb.exec("select Sum(G.[hour] - IIF(S.sHour+G.[hour]>22, (S.sHour+G.[hour]-22),0)) from rezhim R, sved S, grafik G where S.iknum=" + iknum.ToString() + " and R.holiday=1 and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and S.fio='" + fio + "';").Tables[0].Rows[0][0]);

                Double koef = 1.5;
                try
                {
                    koef = Convert.ToDouble(dt.Rows[r][2].ToString());
                }
                catch (Exception) { }
                if (nPost == 0)
                    koef = 0.0;
                buhDataSet.RaschetUIK.Rows.Add(tikName, iknum, period, fio, post, stavka * coef, allHours - (holyHours + nightHours), holyHours + nightHours, koef);
            }
            mdb.close();
            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);//Show as PrintPage
            this.reportViewer1.RefreshReport();
        }
    }
}