namespace buh
{
    partial class SvedUIKreport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SvedUIKreport));
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SvedUIKBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buhDataSet = new buh.buhDataSet();
            ((System.ComponentModel.ISupportInitialize)(this.SvedUIKBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buhDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "buhDataSet_SvedUIK";
            reportDataSource1.Value = this.SvedUIKBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "buh.reports.�������� ���.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ShowBackButton = false;
            this.reportViewer1.ShowContextMenu = false;
            this.reportViewer1.ShowCredentialPrompts = false;
            this.reportViewer1.ShowDocumentMapButton = false;
            this.reportViewer1.ShowFindControls = false;
            this.reportViewer1.ShowRefreshButton = false;
            this.reportViewer1.ShowStopButton = false;
            this.reportViewer1.ShowZoomControl = false;
            this.reportViewer1.Size = new System.Drawing.Size(784, 602);
            this.reportViewer1.TabIndex = 1;
            // 
            // SvedUIKBindingSource
            // 
            this.SvedUIKBindingSource.DataMember = "SvedUIK";
            this.SvedUIKBindingSource.DataSource = this.buhDataSet;
            // 
            // buhDataSet
            // 
            this.buhDataSet.DataSetName = "buhDataSet";
            this.buhDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SvedUIKreport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 602);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SvedUIKreport";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "�������� �� ������������ �������";
            this.Load += new System.EventHandler(this.SvedUIKreport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SvedUIKBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buhDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource SvedUIKBindingSource;
        private buhDataSet buhDataSet;

    }
}