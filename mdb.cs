using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Windows.Forms;

public class mdb {
    private static OleDbConnection con = new OleDbConnection(@"Provider=Microsoft.JET.OLEDB.4.0;data source=buh.mdb;Persist Security Info=False;");// Jet OLEDB:Database Password=myPassword;");
    private static OleDbCommand cmd = new OleDbCommand();
    public static void open()
    {
        con.Open();
        cmd.Connection = con;
    }
    public static DataSet exec(String q)
    {
        cmd.CommandText = q;
        DataSet myDataSet = null;
        if (q.Contains("select"))
        {
            myDataSet = new DataSet();
            OleDbDataAdapter myDataAdapter = new OleDbDataAdapter(cmd);
            myDataAdapter.Fill(myDataSet);
        } else {
            cmd.ExecuteNonQuery();
        }
        return myDataSet;
    }
    public static void close()
    {
        con.Close();
    }
}
