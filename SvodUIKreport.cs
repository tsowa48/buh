using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using Microsoft.Reporting.WinForms;

namespace buh
{
    public partial class SvodUIKreport : Form
    {
        private object[] param;
        public SvodUIKreport()
        {
            InitializeComponent();
        }
        public SvodUIKreport(object[] param)
        {
            InitializeComponent();
            this.param = param;
        }

        private void SvodUIKreport_Load(object sender, EventArgs e)
        {
            //{ uikNum, minMonth, maxMonth, year, period }
            //���������� ������
            int iknum = Convert.ToInt32(this.param[0]);
            //������ (���� ��� �� ��������� �����)
            String period = this.param[4].ToString();
            String sql = "";
            //int rDate = 0;
            String[] month = new String[] { "-", "������", "�������", "����", "������", "���", "����", "����", "������", "��������", "�������", "������", "�������" };
            if (!period.Contains("����"))
            {
                //rDate = period + " " + this.param[3].ToString();
                int CurMonth = Array.IndexOf(month, period);
                sql = " and month(S.[day])=" + CurMonth.ToString();
            }
            mdb.open();
            DataTable dt = mdb.exec("select K.fio from kadry K where K.post=0 and K.iknum="  + iknum.ToString() + ";").Tables[0];//��� ������������
            String pred = dt.Rows[0][0].ToString();
            dt = mdb.exec("select D.[stop] from dates D;").Tables[0];//���� ���������� ��������� (��������� ���� ������)
            DateTime eDay = Convert.ToDateTime(dt.Rows[0][0]);
            double stavka = Convert.ToDouble(mdb.exec("select S.price from stavka S, ik I where I.iknum=" + iknum.ToString() + " and iif(I.voters<1001, 1, iif(I.voters>2000, 3, 2))=S.grp;").Tables[0].Rows[0][0]);

            //��� ����
            dt = mdb.exec("select S.fio, Sum(G.[hour]), month(G.day), K.post from rezhim R, sved S, grafik G, kadry K where S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and S.fio=K.fio and K.iknum=" + iknum.ToString() + sql + " group by S.fio,month(G.day),K.post order by K.post asc;").Tables[0];
            Dictionary<String,double> Lfio = new Dictionary<String,double>();
            for (int cols = 0; cols < dt.Rows.Count; ++cols)
            {
                String fio = dt.Rows[cols][0].ToString();
                String rDate = month[Convert.ToInt32(dt.Rows[cols][2])];
                int allHours = Convert.ToInt32(dt.Rows[cols][1]);
                //����� �� ������ �����
                int nightHours = 0;
                DataTable dt1 = mdb.exec("select G.[hour], S.sHour from rezhim R, sved S, grafik G where S.iknum=" + iknum.ToString() + " and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and ((S.sHour<6 and G.[hour]>0) or (S.sHour+G.[hour])>21) and S.fio='" + fio + "'" + sql + ";").Tables[0];
                for (int r = 0; r < dt1.Rows.Count; ++r)
                {
                    int hour = Convert.ToInt32(dt1.Rows[r][0]);
                    int sHour = Convert.ToInt32(dt1.Rows[r][1]);
                    if ((sHour + hour) > 21)
                        nightHours += (sHour + hour) - 22;
                    else if (sHour < 6 && hour > 0 && (sHour + hour < 7))
                        nightHours += hour;
                    else if (sHour < 6 && hour > 0 && (sHour + hour > 7))
                        nightHours += hour - (sHour + hour - 6);
                }
                //����� �� ��������
                int holyHours = 0;
                holyHours = Convert.ToInt32(mdb.exec("select Sum(G.[hour] - IIF(S.sHour+G.[hour]>22, (S.sHour+G.[hour]-22),0)) from rezhim R, sved S, grafik G where S.iknum=" + iknum.ToString() + " and R.holiday=1 and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and S.fio='" + fio + "'" + sql + ";").Tables[0].Rows[0][0]);

                int post = Convert.ToInt32(dt.Rows[cols][3]);
                double coef = (post == 1 || post == 2) ? 0.9 : (post == 3 ? 0.8 : 1.0);
                double payment = (allHours + nightHours + holyHours) * stavka * coef;//����� �����(�����,����,��������) * ������
                if (Lfio.ContainsKey(fio))
                {
                    double p = Lfio[fio];
                    Lfio.Remove(fio);
                    Lfio.Add(fio, payment + p);
                }
                else
                    Lfio.Add(fio, payment);
                buhDataSet.SvodUIK.Rows.Add(iknum, fio.Replace(" ", "\r\n"), payment, rDate, eDay, pred);
            }
            //�������� ����� � �������
            double sumPayment = 0.0;
            double paymentPred = 0.0;
            double raspredUIK = Convert.ToDouble(mdb.exec("select R.dopopl + R.vozn from raspred R where R.iknum=" + iknum.ToString() + ";").Tables[0].Rows[0][0]);
            foreach (KeyValuePair<String, double> F in Lfio)
            {
                sumPayment += F.Value;
                if (F.Key.Equals(pred))
                    paymentPred = F.Value;
            }
            foreach (KeyValuePair<String,double> F in Lfio)
            {
                double all = F.Value;
                double T8 = (raspredUIK - sumPayment) * 100 / (sumPayment - paymentPred);
                double vozn = all * T8 / 100;
                if (F.Key.Equals(pred))
                    vozn = 0.0;
                buhDataSet.SvodUIK.Rows.Add(iknum, F.Key.Replace(" ", "\r\n"), all, "�����", eDay, pred);
                buhDataSet.SvodUIK.Rows.Add(iknum, F.Key.Replace(" ", "\r\n"), vozn, "��������������", eDay, pred);
                buhDataSet.SvodUIK.Rows.Add(iknum, F.Key.Replace(" ", "\r\n"), all + vozn, "����� � ����������", eDay, pred);
            }
            mdb.close();
            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);//Show as PrintPage
            this.reportViewer1.RefreshReport();
        }
    }
}