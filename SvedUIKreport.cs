using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Globalization;

namespace buh
{
    public partial class SvedUIKreport : Form
    {
        private object[] param;
        public SvedUIKreport()
        {
            InitializeComponent();
        }
        public SvedUIKreport(object[] param)
        {
            InitializeComponent();
            this.param = param;
        }

        private void SvedUIKreport_Load(object sender, EventArgs e)
        {
            //���������� ������
            int iknum = Convert.ToInt32(this.param[0]);
            String pred = "";
            String zam = "";
            String secretar = "";
            //������ (���� ��� �� ��������� �����)
            String period = this.param[4].ToString();
            String sql = "";
            String reportDate = "";
            if (period.Contains("����"))
                reportDate = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(this.param[1])).ToLower() + " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(this.param[2])).ToLower() + " " + this.param[3].ToString() + " ����";
            else
            {
                reportDate = period + " " + this.param[3].ToString() + " ����";
                String[] month = new String[] { "-", "������", "�������", "����", "������", "���", "����", "����", "������", "��������", "�������", "������", "�������" };
                int CurMonth = Array.IndexOf(month, period);
                sql = " and MONTH(S.[day])=" + CurMonth.ToString();
            }
            mdb.open();
            DateTime voteDay = Convert.ToDateTime(mdb.exec("select D.vote from dates D;").Tables[0].Rows[0][0]);//���� �����������
            DataTable dt_rukovod = mdb.exec("select fio from kadry where iknum=" + iknum.ToString() + " and post<3 order by post asc;").Tables[0];
            String[] _fio = dt_rukovod.Rows[0][0].ToString().Split(' ');
            pred = _fio[0] + " " + _fio[1].Substring(0, 1) + ". " + _fio[2].Substring(0, 1) + ".";
            _fio = dt_rukovod.Rows[1][0].ToString().Split(' ');
            zam = _fio[0] + " " + _fio[1].Substring(0, 1) + ". " + _fio[2].Substring(0, 1) + ".";
            _fio = dt_rukovod.Rows[2][0].ToString().Split(' ');
            secretar = _fio[0] + " " + _fio[1].Substring(0, 1) + ". " + _fio[2].Substring(0, 1) + ".";

            DataTable dt = mdb.exec("select S.fio, S.[day], S.sHour, G.[hour], R.holiday, K.post from sved S, kadry K, grafik G, rezhim R where R.[day]=G.[day] and G.iknum=K.iknum and G.fio=S.fio and G.day=S.day and K.iknum=S.iknum and S.iknum=" + iknum.ToString() + " and K.fio=S.fio" + sql + " order by K.post asc, S.[day] asc, S.fio asc;").Tables[0];
            List<String> Lfio = new List<String>();
            for (int cols = 0; cols < dt.Rows.Count; ++cols)
            {
                String fio = dt.Rows[cols][0].ToString();
                Lfio.Add(fio);
                DateTime day = Convert.ToDateTime(dt.Rows[cols][1]);
                int sHour = Convert.ToInt32(dt.Rows[cols][2]);
                int hour = Convert.ToInt32(dt.Rows[cols][3]);
                if (sHour + hour > 24 || (sHour + hour > 22 && day.ToShortDateString() != voteDay.ToShortDateString()))//WARNING (�� ��������� �� ���� ��������� ���������)
                {
                    MessageBox.Show("��������� ���� ������!\n�������� ����� ������ ������ ��� ���������� �����!\n\n���������: " + fio + "\n����: " + day.ToShortDateString() + "\n���������� ����� �������� ���: " + (day.ToShortDateString() == voteDay.ToShortDateString() ? "24" : "22") + ":00");
                    mdb.close();
                    return;
                }
                String HourRow = (hour > 0 ? (sHour.ToString() + ":00-" + Convert.ToString(sHour + hour) + ":00\r\n" + hour.ToString() + "� (�)") : "");
                String holyChar = ((day.DayOfWeek == DayOfWeek.Saturday && Convert.ToInt32(dt.Rows[cols][4]) == 1) ? " (�)" : ((day.DayOfWeek == DayOfWeek.Sunday && Convert.ToInt32(dt.Rows[cols][4]) == 1) ? " (�)" : (Convert.ToInt32(dt.Rows[cols][4]) == 1 ? " (�)" : "")));
                buhDataSet.SvedUIK.Rows.Add(iknum, day.ToString("d MMMM") + holyChar, fio.Replace(" ", "\r\n"), sHour, hour, pred, zam, secretar, reportDate, HourRow);
            }

            //�������� ������ � �������
            foreach(String fio in Lfio)
            {
                //����� �����
                dt = mdb.exec("select Sum(G.[hour]) from rezhim R, sved S, grafik G where S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and S.fio='" + fio + "'" + sql + ";").Tables[0];
                buhDataSet.SvedUIK.Rows.Add(iknum, "���������� �����, �����", fio.Replace(" ", "\r\n"), 0, 0, "", "", "", "", dt.Rows[0][0].ToString());

                //��� ������� �����������
                buhDataSet.SvedUIK.Rows.Add(iknum, "�� ���:\r\n1. ���\r\n������� �����������", fio.Replace(" ", "\r\n"), 0, 0, "", "", "", "", "0");

                //��� ���.������ �����
                buhDataSet.SvedUIK.Rows.Add(iknum, "2. ���\r\n�������-������� ������ ����� (�����������-���), �����", fio.Replace(" ", "\r\n"), 0, 0, "", "", "", "", dt.Rows[0][0].ToString());

                //��������� ����� �� ������ �����
                dt = mdb.exec("select G.[hour], S.sHour from rezhim R, sved S, grafik G where S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and ((S.sHour<6 and G.[hour]>0) or (S.sHour+G.[hour])>21) and S.fio='" + fio + "'" + sql + ";").Tables[0];
                int sum = 0;
                for (int r = 0; r < dt.Rows.Count; ++r)
                {
                    int hour = Convert.ToInt32(dt.Rows[r][0]);
                    int sHour = Convert.ToInt32(dt.Rows[r][1]);
                    if ((sHour + hour) > 21)
                        sum += (sHour + hour) - 22;
                    else if (sHour < 6 && hour > 0 && (sHour + hour < 7))
                        sum += hour;
                    else if (sHour < 6 && hour > 0 && (sHour + hour > 7))
                        sum += hour - (sHour + hour - 6);
                }
                buhDataSet.SvedUIK.Rows.Add(iknum, "� ��� �����:\r\n� ������ �����", fio.Replace(" ", "\r\n"), 0, 0, "", "", "", "", sum.ToString());

                //��������� ����� �� ��������
                dt = mdb.exec("select Sum(G.[hour] - IIF(S.sHour+G.[hour]>22, (S.sHour+G.[hour]-22),0)) from rezhim R, sved S, grafik G where R.holiday=1 and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and S.fio='" + fio + "'" + sql + ";").Tables[0];
                buhDataSet.SvedUIK.Rows.Add(iknum, "� �������� � ��������� ����������� ���", fio.Replace(" ", "\r\n"), 0, 0, "", "", "", "", dt.Rows[0][0].ToString());
            }

            mdb.close();
            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);//Show as PrintPage
            this.reportViewer1.RefreshReport();
        }
    }
}