using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace buh
{
    public partial class DopOplataUIKreport : Form
    {
        private object[] param;

        public DopOplataUIKreport(object[] param)
        {
            InitializeComponent();
            this.param = param;
        }

        private void DopOplataUIKreport_Load(object sender, EventArgs e)
        {
            int iknum = Convert.ToInt32(this.param[0]);
            int VEDOMOST = Convert.ToInt32(this.param[1]);

            mdb.open();
            String[] month = new String[] { "-", "������", "�������", "����", "������", "���", "����", "����", "������", "��������", "�������", "������", "�������" };
            DataTable ds = mdb.exec("select D.stop, month(D.start), month(D.stop), year(D.stop) from dates D;").Tables[0];
            DateTime eDay = Convert.ToDateTime(ds.Rows[0][0]);
            string period = month[Convert.ToInt32(ds.Rows[0][1])] + " - " + month[Convert.ToInt32(ds.Rows[0][2])] + " " + ds.Rows[0][3].ToString() + "�.";
            double stavka = Convert.ToDouble(mdb.exec("select S.price from stavka S, ik I where I.iknum=" + iknum.ToString() + " and iif(I.voters<1001, 1, iif(I.voters>2000, 3, 2))=S.grp;").Tables[0].Rows[0][0]);
            DataTable dt = mdb.exec("select S.fio, Sum(G.[hour]), K.post, K.koef from rezhim R, sved S, grafik G, kadry K where S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and S.fio=K.fio and K.iknum=" + iknum.ToString() + " group by S.fio,K.post,K.koef order by K.post asc, S.fio asc;").Tables[0];
            String[] pred = dt.Rows[0][0].ToString().Split(' ');
            if (VEDOMOST == 1)
            {
                this.Text = "�������������� ������ ����� (��������������)";
                for (int cols = 0; cols < dt.Rows.Count; ++cols)
                {
                    String fio = dt.Rows[cols][0].ToString();
                    int post = Convert.ToInt32(dt.Rows[cols][2]);
                    int allHours = Convert.ToInt32(dt.Rows[cols][1]);
                    //����� �� ������ �����
                    int nightHours = 0;
                    DataTable dt1 = mdb.exec("select G.[hour], S.sHour from rezhim R, sved S, grafik G where S.iknum=" + iknum.ToString() + " and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and ((S.sHour<6 and G.[hour]>0) or (S.sHour+G.[hour])>21) and S.fio='" + fio + "';").Tables[0];
                    for (int r = 0; r < dt1.Rows.Count; ++r)
                    {
                        int hour = Convert.ToInt32(dt1.Rows[r][0]);
                        int sHour = Convert.ToInt32(dt1.Rows[r][1]);
                        if ((sHour + hour) > 21)
                            nightHours += (sHour + hour) - 22;
                        else if (sHour < 6 && hour > 0 && (sHour + hour < 7))
                            nightHours += hour;
                        else if (sHour < 6 && hour > 0 && (sHour + hour > 7))
                            nightHours += hour - (sHour + hour - 6);
                    }
                    //����� �� ��������
                    int holyHours = 0;
                    holyHours = Convert.ToInt32(mdb.exec("select Sum(G.[hour] - IIF(S.sHour+G.[hour]>22, (S.sHour+G.[hour]-22),0)) from rezhim R, sved S, grafik G where S.iknum=" + iknum.ToString() + " and R.holiday=1 and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and S.fio='" + fio + "';").Tables[0].Rows[0][0]);
                    double coef = (post == 1 || post == 2) ? 0.9 : (post == 3 ? 0.7 : 1.0);
                    double payment = (allHours + nightHours + holyHours) * stavka * coef;//����� �����(�����,����,��������) * ������
                    buhDataSet.DopOplUIK.Rows.Add(iknum, eDay, pred[0] + " " + pred[1].Substring(0, 1) + ". " + pred[2].Substring(0, 1) + ".", fio, (post == 0 ? "������������" : (post == 1 ? "���. ������������" : (post == 2 ? "���������" : "���� ��������"))), payment, VEDOMOST, period);
                }
            }
            else if (VEDOMOST == 2)
            {
                this.Text = "�������������� ������ ����� (�� �������� ������)";
                for (int cols = 0; cols < dt.Rows.Count; ++cols)
                {
                    String fio = dt.Rows[cols][0].ToString();
                    int post = Convert.ToInt32(dt.Rows[cols][2]);
                    if (post == 0)
                        continue;
                    int allHours = Convert.ToInt32(dt.Rows[cols][1]);
                    double vedomKoef = 0.0;
                    try
                    {
                        vedomKoef = Convert.ToDouble(dt.Rows[cols][3]);
                    }
                    catch (Exception) { }
                    //����� �� ������ �����
                    int nightHours = 0;
                    DataTable dt1 = mdb.exec("select G.[hour], S.sHour from rezhim R, sved S, grafik G where S.iknum=" + iknum.ToString() + " and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and ((S.sHour<6 and G.[hour]>0) or (S.sHour+G.[hour])>21) and S.fio='" + fio + "';").Tables[0];
                    for (int r = 0; r < dt1.Rows.Count; ++r)
                    {
                        int hour = Convert.ToInt32(dt1.Rows[r][0]);
                        int sHour = Convert.ToInt32(dt1.Rows[r][1]);
                        if ((sHour + hour) > 21)
                            nightHours += (sHour + hour) - 22;
                        else if (sHour < 6 && hour > 0 && (sHour + hour < 7))
                            nightHours += hour;
                        else if (sHour < 6 && hour > 0 && (sHour + hour > 7))
                            nightHours += hour - (sHour + hour - 6);
                    }
                    //����� �� ��������
                    int holyHours = 0;
                    holyHours = Convert.ToInt32(mdb.exec("select Sum(G.[hour] - IIF(S.sHour+G.[hour]>22, (S.sHour+G.[hour]-22),0)) from rezhim R, sved S, grafik G where S.iknum=" + iknum.ToString() + " and R.holiday=1 and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and S.fio='" + fio + "';").Tables[0].Rows[0][0]);
                    double coef = (post == 1 || post == 2) ? 0.9 : (post == 3 ? 0.7 : 1.0);
                    double payment = (allHours + nightHours + holyHours) * stavka * coef * vedomKoef;//����� �����(�����,����,��������) * ������ * �������������_����.
                    buhDataSet.DopOplUIK.Rows.Add(iknum, eDay, pred[0] + " " + pred[1].Substring(0, 1) + ". " + pred[2].Substring(0, 1) + ".", fio, (post == 0 ? "������������" : (post == 1 ? "���. ������������" : (post == 2 ? "���������" : "���� ��������"))), payment, VEDOMOST, period);
                }
                /*Dictionary<String, KeyValuePair<int, double>> Lfio = new Dictionary<String, KeyValuePair<int, double>>();
                for (int cols = 0; cols < dt.Rows.Count; ++cols)
                {
                    String fio = dt.Rows[cols][0].ToString();
                    //String rDate = "";//month[Convert.ToInt32(dt.Rows[cols][2])];
                    int allHours = Convert.ToInt32(dt.Rows[cols][1]);
                    //����� �� ������ �����
                    int nightHours = 0;
                    DataTable dt1 = mdb.exec("select G.[hour], S.sHour from rezhim R, sved S, grafik G where S.iknum=" + iknum.ToString() + " and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and ((S.sHour<6 and G.[hour]>0) or (S.sHour+G.[hour])>21) and S.fio='" + fio + "';").Tables[0];
                    for (int r = 0; r < dt1.Rows.Count; ++r)
                    {
                        int hour = Convert.ToInt32(dt1.Rows[r][0]);
                        int sHour = Convert.ToInt32(dt1.Rows[r][1]);
                        if ((sHour + hour) > 21)
                            nightHours += (sHour + hour) - 22;
                        else if (sHour < 6 && hour > 0 && (sHour + hour < 7))
                            nightHours += hour;
                        else if (sHour < 6 && hour > 0 && (sHour + hour > 7))
                            nightHours += hour - (sHour + hour - 6);
                    }
                    //����� �� ��������
                    int holyHours = 0;
                    holyHours = Convert.ToInt32(mdb.exec("select Sum(G.[hour] - IIF(S.sHour+G.[hour]>22, (S.sHour+G.[hour]-22),0)) from rezhim R, sved S, grafik G where S.iknum=" + iknum.ToString() + " and R.holiday=1 and S.[day]=R.[day] and G.[day]=S.[day] and S.fio=G.fio and S.fio='" + fio + "';").Tables[0].Rows[0][0]);

                    int post = Convert.ToInt32(dt.Rows[cols][2]);
                    double coef = (post == 1 || post == 2) ? 0.9 : (post == 3 ? 0.7 : 1.0);
                    double payment = (allHours + nightHours + holyHours) * stavka * coef;//����� �����(�����,����,��������) * ������
                    if (Lfio.ContainsKey(fio))
                    {
                        double p = Lfio[fio].Value;
                        Lfio.Remove(fio);
                        Lfio.Add(fio, new KeyValuePair<int, double>(post, payment + p));
                    }
                    else
                        Lfio.Add(fio, new KeyValuePair<int, double>(post, payment));
                }
                //�������� ����� � �������
                double sumPayment = 0.0;
                double paymentPred = 0.0;
                double raspredUIK = Convert.ToDouble(mdb.exec("select R.dopopl + R.vozn from raspred R where R.iknum=" + iknum.ToString() + ";").Tables[0].Rows[0][0]);
                foreach (KeyValuePair<String, KeyValuePair<int, double>> F in Lfio)
                {
                    sumPayment += F.Value.Value;
                    if (F.Key.Equals(pred))
                        paymentPred = F.Value.Value;
                }
                foreach (KeyValuePair<String, KeyValuePair<int, double>> F in Lfio)
                {
                    double all = F.Value.Value;
                    int post = F.Value.Key;
                    double T8 = (raspredUIK - sumPayment) * 100 / (sumPayment - paymentPred);
                    double vozn = all * T8 / 100;
                    if (F.Key.Equals(String.Join(" ",pred)))
                        continue;
                    buhDataSet.DopOplUIK.Rows.Add(iknum, eDay, pred[0] + " " + pred[1].Substring(0, 1) + ". " + pred[2].Substring(0, 1) + ".", F.Key, (post == 1 ? "���. ������������" : (post == 2 ? "���������" : "���� ��������")), vozn, VEDOMOST, period);
                }//*/
            }
            mdb.close();
            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);//Show as PrintPage
            this.reportViewer1.RefreshReport();
        }
    }
}