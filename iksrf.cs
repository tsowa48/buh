using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Reporting.WinForms;

namespace buh
{
    public partial class iksrf : Form
    {
        const String TITLE = "��������� �����";
        public iksrf() {
            InitializeComponent();
        }
        private void iksrf_Load(object sender, EventArgs e) {
            mdb.open();
            //�������� �������� ���
            DialogResult result = MessageBox.Show("��������� �������� � ������ ���?", TITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DialogResult.Yes == result)
            {
                openFileDialog1.Filter = "*.csv|*.csv";
                openFileDialog1.FileName = "22_53_UIK_krat1.csv";
                DialogResult res = openFileDialog1.ShowDialog();
                if (DialogResult.OK == res)
                {
                    String[] lines = File.ReadAllLines(openFileDialog1.FileName, Encoding.Default);
                    Array.Copy(lines, 7, lines, 0, lines.Length - 7);
                    mdb.exec("delete from kadry;");
                    //String S = "insert into kadry(iknum,fio,post) values ";
                    foreach (String L in lines)
                    {
                        String[] line = L.Split(';');
                        int post = (line[2].ToLower().Contains("������������") ? 0 : (line[2].ToLower().Contains("���") ? 1 : (line[2].ToLower().Contains("���������") ? 2 : 3)));
                        //S += "(" + line[1] + ",'" + line[3] + "'," + Convert.ToString(post) + "),";
                        mdb.exec("insert into kadry(iknum,fio,post) values (" + line[1] + ",'" + line[3] + "'," + Convert.ToString(post) + ");");
                    }
                    //S = S.Substring(0, S.Length - 1) + ";";
                    //mdb.exec(S);
                    MessageBox.Show("������� ��� ������� ���������", TITLE, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            //��������� �����������
            result = MessageBox.Show("��������� �������� � ����������� �����������?", TITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DialogResult.Yes == result)
            {
                openFileDialog1.Filter = "*.csv|*.csv";
                openFileDialog1.FileName = "";
                DialogResult res = openFileDialog1.ShowDialog();
                if (res == DialogResult.OK)
                {
                    String[] lines = File.ReadAllLines(openFileDialog1.FileName, Encoding.Default);
                    Array.Copy(lines, 11, lines, 0, lines.Length - 11);
                    mdb.exec("delete from ik;");
                    foreach (String L in lines)
                    {
                        String[] line = L.Split(';');//6 - � ���, 12 - �����������
                        int ik = 0;
                        try { ik = Convert.ToInt32(line[6]); }
                        catch (Exception) { continue; }
                        int voters = Convert.ToInt32(line[12]);
                        mdb.exec("insert into ik(iknum,voters) values (" + Convert.ToString(ik) + "," + Convert.ToString(voters) + ");");
                    }
                    MessageBox.Show("�������� � ����������� ����������� ������� ���������", TITLE, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            //���������� ������ �� �������
            result = MessageBox.Show("�������� ������ ��� ����� ��� �1, �2 � �3 ?", TITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DialogResult.Yes == result)
            {
                util.InputTextBox box = new util.InputTextBox();
                box.Text = TITLE;
                Double group1 = 0.0;
                Double group2 = 0.0;
                Double group3 = 0.0;
                box.label1.Text = "������ ��� ������ ��� �1:";
                if (DialogResult.OK == box.ShowDialog())
                    group1 = Convert.ToDouble(box.textBox1.Text);
                box.textBox1.Text = "";
                box.label1.Text = "������ ��� ������ ��� �2:";
                if (DialogResult.OK == box.ShowDialog())
                    group2 = Convert.ToDouble(box.textBox1.Text);
                box.textBox1.Text = "";
                box.label1.Text = "������ ��� ������ ��� �3:";
                if (DialogResult.OK == box.ShowDialog())
                    group3 = Convert.ToDouble(box.textBox1.Text);
                try
                {
                    mdb.exec("delete from stavka;");
                    mdb.exec("insert into stavka(grp, price) values (1," + Convert.ToString(group1) + ");");
                    mdb.exec("insert into stavka(grp, price) values (2," + Convert.ToString(group2) + ");");
                    mdb.exec("insert into stavka(grp, price) values (3," + Convert.ToString(group3) + ");");
                }
                catch (Exception) { }
            }

            //���������� ���� �����������, ������ � ����� ������ ��������
            bool isVoteDateChanged = false;
            result = MessageBox.Show("�������� ���� ����������� � ������ ���?", TITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DialogResult.Yes == result)
            {
                util.InputTextBox box = new util.InputTextBox();
                box.Text = TITLE;
                DateTime vote = DateTime.MinValue;
                DateTime start = DateTime.MinValue;
                DateTime stop = DateTime.MinValue;

                box.label1.Text = "���� ����������� (��.��.����):";
                if (DialogResult.OK == box.ShowDialog())
                    vote = Convert.ToDateTime(box.textBox1.Text);
                box.textBox1.Text = "";
                box.label1.Text = "���� ������ ������ ��� (��.��.����):";
                if (DialogResult.OK == box.ShowDialog())
                    start = Convert.ToDateTime(box.textBox1.Text);
                box.textBox1.Text = "";
                box.label1.Text = "���� ��������� ������ ��� (��.��.����):";
                if (DialogResult.OK == box.ShowDialog())
                    stop = Convert.ToDateTime(box.textBox1.Text);
                try
                {
                    mdb.exec("delete from dates;");
                    mdb.exec("insert into dates(vote,[start],[stop]) values ('" + vote.ToShortDateString() + "', '" + start.ToShortDateString() + "', '" + stop.ToShortDateString() + "');");
                    //�������� ������ � rezhim
                    mdb.exec("delete from rezhim;");
                    for (DateTime x = start; x < stop.AddDays(1); x = x.AddDays(1))
                    {
                        String X = x.ToShortDateString();
                        listBox1.Items.Add(X);
                        mdb.exec("insert into rezhim([day],holiday) values ('" + X + "',0);");
                    }
                    isVoteDateChanged = true;
                }
                catch (Exception) { }
            }

            //���������� ��� ���
            result = MessageBox.Show("�������� ��� ���?", TITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DialogResult.Yes == result)
            {
                util.InputTextBox box = new util.InputTextBox();
                box.Text = TITLE;
                box.label1.Text = "���� ������ ����� ���:";
                Double fot = 0.0;
                if (DialogResult.OK == box.ShowDialog())
                {
                    fot = Convert.ToDouble(box.textBox1.Text);
                    mdb.exec("delete from raspred;");
                    mdb.exec("insert into raspred(iknum,dopopl,vozn) values (0," + fot.ToString().Replace(",", ".") + ",0);");
                }
            }
            
            //���������� ����������� � ������� ���
            if (!isVoteDateChanged)
            {
                try
                {
                    DataSet ds = mdb.exec("select [day],holiday from rezhim order by [day] asc;");
                    int index = 0;
                    while (index < ds.Tables[0].Rows.Count)
                    {
                        listBox1.Items.Add(Convert.ToDateTime(ds.Tables[0].Rows[index][0]).ToShortDateString());
                        listBox1.SetSelected(index, Convert.ToInt32(ds.Tables[0].Rows[index][1]) == 1);
                        ++index;
                    }
                }
                catch (Exception) { }
            }

            //���������� ���������� ����� �� �������, ���������� � ����
            int[] rabchas = new int[24];
            try
            {
                DataSet ds = mdb.exec("select hour from rabchas order by post asc, day asc, grp asc;");
                for (int i = 0; i < 24; ++i)
                    rabchas[i] = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
            }
            catch (Exception) { for (int i = 0; i < 24; ++i) rabchas[i] = -1; }
            dataGridView1.Rows.Add(new object[] { "������������", "������", rabchas[0], rabchas[1], rabchas[2] });
            dataGridView1.Rows.Add(new object[] { "", "��������", rabchas[3], rabchas[4], rabchas[5] });
            dataGridView1.Rows.Add(new object[] { "�����������", "������", rabchas[6], rabchas[7], rabchas[8] });
            dataGridView1.Rows.Add(new object[] { "", "��������", rabchas[9], rabchas[10], rabchas[11] });
            dataGridView1.Rows.Add(new object[] { "���������", "������", rabchas[12], rabchas[13], rabchas[14] });
            dataGridView1.Rows.Add(new object[] { "", "��������", rabchas[15], rabchas[16], rabchas[17] });
            dataGridView1.Rows.Add(new object[] { "���� ���", "������", rabchas[18], rabchas[19], rabchas[20] });
            dataGridView1.Rows.Add(new object[] { "", "��������", rabchas[21], rabchas[22], rabchas[23] });

            mdb.close();
        }
        private void iksrf_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            mdb.open();
            //��������� ��������� � ����������� ����
            mdb.exec("update rezhim set holiday=0;");
            for (int i = 0; i < listBox1.SelectedItems.Count; ++i)
                mdb.exec("update rezhim set holiday=1 where [day]=Format('" + listBox1.SelectedItems[i].ToString() + "','Short Date',2);");
            
            //��������� ��������� � ����� ������
            mdb.exec("delete from rabchas;");
            for (int i = 0; i < 8; i += 2)
            {
                int post = i / 2;
                int hour1 = Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value);
                int hour2 = Convert.ToInt32(dataGridView1.Rows[i].Cells[3].Value);
                int hour3 = Convert.ToInt32(dataGridView1.Rows[i].Cells[4].Value);
                int hour1h = Convert.ToInt32(dataGridView1.Rows[i + 1].Cells[2].Value);
                int hour2h = Convert.ToInt32(dataGridView1.Rows[i + 1].Cells[3].Value);
                int hour3h = Convert.ToInt32(dataGridView1.Rows[i + 1].Cells[4].Value);
                mdb.exec("insert into rabchas(post,[day],grp,[hour]) values (" + Convert.ToString(post) + ",0,1," + Convert.ToString(hour1) + ");");
                mdb.exec("insert into rabchas(post,[day],grp,[hour]) values (" + Convert.ToString(post) + ",0,2," + Convert.ToString(hour2) + ");");
                mdb.exec("insert into rabchas(post,[day],grp,[hour]) values (" + Convert.ToString(post) + ",0,3," + Convert.ToString(hour3) + ");");
                mdb.exec("insert into rabchas(post,[day],grp,[hour]) values (" + Convert.ToString(post) + ",1,1," + Convert.ToString(hour1h) + ");");
                mdb.exec("insert into rabchas(post,[day],grp,[hour]) values (" + Convert.ToString(post) + ",1,2," + Convert.ToString(hour2h) + ");");
                mdb.exec("insert into rabchas(post,[day],grp,[hour]) values (" + Convert.ToString(post) + ",1,3," + Convert.ToString(hour3h) + ");");
            }
            mdb.close();
        }

        private void ����������������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new RaspredTIKreport()).ShowDialog();
        }
        private void listBox1_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            button1.Enabled = true;
        }
    }
}