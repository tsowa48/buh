using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Globalization;

namespace buh
{
    public partial class GrafikUIKreport : Form
    {
        private object[] param;

        public GrafikUIKreport()
        {
            InitializeComponent();
        }
        public GrafikUIKreport(object[] param)
        {
            InitializeComponent();
            this.param = param;
        }

        private void GrafikUIKreport_Load(object sender, EventArgs e)
        {
            int iknum = Convert.ToInt32(this.param[0]);
            //��� ���������
            String secretar = "";
            //���� ������ (etc: �������-���� 2018 ����)
            String reportDate = "";
            //������ (���� ��� �� ��������� �����)
            String period = this.param[4].ToString();
            String sql = "";
            if (period.Contains("����"))
                reportDate = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(this.param[1])).ToLower() + " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(this.param[2])).ToLower() + " " + this.param[3].ToString() + " ����";
            else
            {
                reportDate = period + " " + this.param[3].ToString() + " ����";
                String[] month = new String[] { "-", "������", "�������", "����", "������", "���", "����", "����", "������", "��������", "�������", "������", "�������" };
                int CurMonth = Array.IndexOf(month, period);
                sql = " and MONTH(day)=" + CurMonth.ToString();
            }

            mdb.open();
            String[] _fio = mdb.exec("select fio from kadry where iknum=" + iknum.ToString() + " and post=2;").Tables[0].Rows[0][0].ToString().Split(' ');
            secretar = _fio[0] + " " + _fio[1].Substring(0, 1) + ". " + _fio[2].Substring(0, 1) + ".";
            DateTime st = Convert.ToDateTime(mdb.exec("select [start] from dates;").Tables[0].Rows[0][0]);
            DataTable dt = mdb.exec("select G.fio, G.[day], G.[hour], K.post from grafik G, kadry K where K.iknum=G.iknum and G.iknum=" + iknum.ToString() + " and K.fio=G.fio" + sql + " order by K.post asc, G.[day] asc, G.fio asc;").Tables[0];
            for (int cols = 0; cols < dt.Rows.Count; ++cols)
            {
                String fio = dt.Rows[cols][0].ToString();
                DateTime day = Convert.ToDateTime(dt.Rows[cols][1]);
                int hour = Convert.ToInt32(dt.Rows[cols][2]);
                buhDataSet.GrafikUIK.Rows.Add(iknum, day, fio.Replace(" ","\r\n"), hour, st, reportDate, secretar);
            }
            mdb.close();

            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);//Show as PrintPage
            this.reportViewer1.RefreshReport();
        }
    }
}